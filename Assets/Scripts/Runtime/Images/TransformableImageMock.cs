﻿using DigitalSputnikTestTask.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Component that handles the object that represents a transformable image when dragging from the add picture menu to the gallery.
    /// </summary>
    public class TransformableImageMock : MonoBehaviour
    {
        #region Properties

        /// <summary>
        /// Apps's main camera.
        /// </summary>
        public static Camera MainCamera { get; set; }

        /// <summary>     
        /// Scriptable object that holds configuration data for a <see cref="TransformableImage"/>.
        /// </summary>
        public TransformableImageConfig TransformableImageConfig { get; set; } = null;

        #endregion

        #region Fields

        /// <summary>
        /// GUI component that draws the mock image when we're dragging it from the gallery menu into the scene.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("GUI component that draws the mock image when we're dragging it from the gallery menu into the scene.")]
        [SerializeField]
        private Image image = null;

        /// <summary>
        /// RectTransform component on the slide out menu.
        /// </summary>
        [Tooltip("RectTransform component on the slide out menu.")]        
        [SerializeField]
        private RectTransform slideOutMenuRectTransform = null;

        /// <summary>
        /// Scriptable object that holds the position of the player's input on current frame.
        /// </summary>
        [Space]
        [Header("Assets")]
        [Space]
        [Tooltip("Scriptable object that holds the position of the player's input on current frame.")]
        [SerializeField]
        private Vector2Variable currentInputPosition = null;

        /// <summary>
        /// Scriptable object that holds the position of the player's input on previous frame.
        /// </summary>
        [Tooltip("Scriptable object that holds the position of the player's input on previous frame.")]
        [SerializeField]
        private Vector2Variable previousInputPosition = null;

        /// <summary>
        /// Is the mock icon currently being dragged?
        /// </summary>
        private bool isBeingDragged = false;

        #endregion

        #region Toggle

        /// <summary>
        /// Enables the mock image.
        /// </summary>
        public void Enable(TransformableImageConfig transformableImageConfig)
        {
            TransformableImageConfig = transformableImageConfig;
            transform.position = currentInputPosition.Value;
            image.sprite = TransformableImageConfig.GallerySprite;
            SetImageAlpha(1f);
            isBeingDragged = true;
        }

        /// <summary>
        /// Disables the mock image.
        /// </summary>
        public void Disable()
        {
            SetImageAlpha(0f);
            isBeingDragged = false;
        }

        #endregion

        #region Color

        /// <summary>
        /// Sets the alpha value on mock's image.
        /// </summary>
        /// <param name="targetAlpha">Target alpha value.</param>
        private void SetImageAlpha(float targetAlpha)
        {
            Color newColor = image.color;
            newColor.a = targetAlpha;
            image.color = newColor;
        }

        #endregion

        #region Drag

        public void Update()
        {
            if (!isBeingDragged)
            {
                return;
            }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            if (!Input.GetMouseButton(0))
            {
                Disable();
                return;
            }
#else
            if (Input.touchCount == 0)
            {
                Debug.Log("Disabling 2");
                Disable();
                return;
            }
#endif
            transform.position += (Vector3)(currentInputPosition.Value - previousInputPosition.Value);            

            if (!RectTransformUtility.RectangleContainsScreenPoint(slideOutMenuRectTransform, transform.TransformPoint(image.sprite.bounds.center)))
            {
                TransformableImage.DraggedImage = ImageManager.SpawnTransformableImage(
                                                  TransformableImageConfig, MainCamera.ScreenToWorldPoint(currentInputPosition.Value));
                Disable();
            }
        }

        #endregion
    }
}