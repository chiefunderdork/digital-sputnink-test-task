﻿using DigitalSputnikTestTask.ScriptableObjects;
using DigitalSputnikTestTask.Events;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Controls operations of an image handle that allows to scale and rotate a <see cref="TransformableImage"/>.
    /// </summary>
    public class TransformableImageHandle : MonoBehaviour
    {
        #region Properties

        /// <summary>
        /// Game's main camera.
        /// </summary>
        public static Camera MainCamera { get; set; }

        /// <summary>
        /// Scriptable object that holds the position of the player's input on current frame.
        /// </summary>
        public static Vector2Variable CurrentInputPosition { get; set; }

        /// <summary>
        /// Collider to detect handle touches.
        /// </summary>
        /// <value>
        /// Gets the value of the field handleCollider.
        /// </value>
        public Collider2D HandleCollider => handleCollider;

        /// <summary>
        /// Is handle currently being dragged?
        /// </summary>
        public bool IsBeingDragged { get; set; } = false;

        #endregion

        #region Fields

        /// <summary>
        /// Collider to detect handle touches.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Collider to detect handle touches.")]
        [SerializeField]
        private Collider2D handleCollider = null;

        /// <summary>
        /// Component that controls the behavior of a transformable image.
        /// </summary>
        [Tooltip("Component that controls the behavior of a transformable image.")]
        [SerializeField]
        private TransformableImage transformableImage = null;

        /// <summary>
        /// Transform that marks the image center.
        /// </summary>
        [Tooltip("Transform that marks the image center.")]
        [SerializeField]
        private Transform imageCenter = null;

        /// <summary>
        /// Event that is raised when user starts dragging this image.
        /// </summary>
        [Space]
        [Header("Events")]
        [Space]
        [Tooltip("Event that is raised when user starts dragging this image.")]
        [SerializeField]
        private GameEvent onStartedDraggingImage = null;

        /// <summary>
        /// Event that is raised when user stops dragging this image.
        /// </summary>
        [Tooltip("Event that is raised when user stops dragging this image.")]
        [SerializeField]
        private GameEvent onStoppedDraggingImage = null;

        /// <summary>
        /// Handle that lies across the diagonal from this one.
        /// </summary>
        [SerializeField]
        private TransformableImageHandle oppositeHandle = null;

        /// <summary>
        /// Vector going from the center of the image to the corner on previous frame.
        /// </summary>
        private Vector3 oldVectorFromCenterToCorner;

        /// <summary>
        /// Image rotation when it started rotating.
        /// </summary>
        private Quaternion startRotation;

        /// <summary>
        /// Image scale when it started scaling.
        /// </summary>
        private Vector3 startScale;

        #endregion

        #region Transform

        #region Update

        private void Update()
        {
            if (!IsBeingDragged)
            {
                DragCheck();
            }
            else
            {
                OnDrag();
            }
        }

        #endregion

        /// <summary>
        /// Checks if the user has started dragging this handle and sets it up accordingly.
        /// </summary>
        private void DragCheck()
        {
            if (SaveGalleryMenu.IsEnabled || LoadGalleryMenu.IsEnabled)
            {
                return;
            }

            bool startDrag = false;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            startDrag = TransformableImage.DraggedImage == null && !IsBeingDragged && (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0))
                        && handleCollider.OverlapPoint(MainCamera.ScreenToWorldPoint(Input.mousePosition)) 
                        && !ImageManager.HandleOnOneOfTheImagesIsBeingDragged;
#else
            startDrag = TransformableImage.DraggedImage == null && !IsBeingDragged && Input.touchCount > 0 
                        && handleCollider.OverlapPoint(MainCamera.ScreenToWorldPoint(Input.GetTouch(0).position))
                        && !ImageManager.HandleOnOneOfTheImagesIsBeingDragged;
#endif
            if (startDrag)
            {
                TransformableImage.InteractionHint.transform.position = MainCamera.WorldToScreenPoint(transform.position);
                TransformableImage.InteractionHint.enabled = true;

                onStartedDraggingImage.Raise();
                UpdateVectorFromCenterToCorner();
                startRotation = transformableImage.transform.rotation;
                startScale = transform.transform.localScale;

                IsBeingDragged = true;
            }
        }

        /// <summary>
        /// Performs transform operations while the handle is being dragged.
        /// </summary>
        private void OnDrag()
        {
            if (SaveGalleryMenu.IsEnabled || LoadGalleryMenu.IsEnabled)
            {
                return;
            }

            if (TransformableImage.DraggedImage != null)
            {
                EndDrag();
                return;
            }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            if (!Input.GetMouseButton(0))
            {
                EndDrag();
                return;
            }
#else
            if (Input.touchCount == 0)
            {
                EndDrag();
                return;
            }
#endif
            foreach (TransformableImageHandle transformableImageHandle in transformableImage.TransformableImageHandles)
            {
                if (transformableImage.HandleHasReachedMenuPanel(transformableImageHandle))
                {
                    Vector3 slideOutPanelEdgeMarkerPosition = MainCamera.ScreenToWorldPoint(TransformableImage.SlideOutPanelEdgeMarker.position);

                    float penetration = Mathf.Abs(transformableImageHandle.transform.position.x - slideOutPanelEdgeMarkerPosition.x);

                    transformableImage.transform.position += Vector3.right * (penetration + 0.01f);
                    return;
                }
            }

            TransformableImage.InteractionHint.transform.position = MainCamera.WorldToScreenPoint(transform.position);

            Vector3 inputWorldPosition = MainCamera.ScreenToWorldPoint(CurrentInputPosition.Value);
            inputWorldPosition.z = 0f;

            float angle = Vector3.SignedAngle(inputWorldPosition - imageCenter.position, oldVectorFromCenterToCorner, Vector3.forward);

            transformableImage.Rotate(-angle);
            UpdateVectorFromCenterToCorner();

            float diagonalLength = Vector3.Distance(inputWorldPosition, oppositeHandle.transform.position);
            transformableImage.Scale(diagonalLength);
        }

        /// <summary>
        /// Resets the object and ads the drag action to the undo/redo stack when the drag ends.
        /// </summary>
        private void EndDrag()
        {
            IsBeingDragged = false;
            UndoRedoStack.AddAction(new RotateAndScaleUndoRedoAction(startRotation, transformableImage.transform.rotation, startScale,
                                                                     transformableImage.transform.localScale, UndoRedoActionType.RotateAndScale,
                                                                     transformableImage.transform));
            onStoppedDraggingImage.Raise();
            TransformableImage.InteractionHint.enabled = false;            
        }

        /// <summary>
        /// Updates the value in <see cref="oldVectorFromCenterToCorner"/>.
        /// </summary>
        private void UpdateVectorFromCenterToCorner() => oldVectorFromCenterToCorner = transform.position - imageCenter.position;

        #endregion
    }
}