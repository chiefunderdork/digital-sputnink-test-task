﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Toggles an array of component on and off.
    /// </summary>
    public class ComponentToggle : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Components to toggle.
        /// </summary>
        [Tooltip("Component to toggle.")]
        [SerializeField]
        private MonoBehaviour[] componentsToToggle = null;

        #endregion

        #region Toggle

        /// <summary>
        /// Enables target components.
        /// </summary>
        public void EnableComponents()
        {
            foreach (MonoBehaviour componentToToggle in componentsToToggle)
            {
                componentToToggle.enabled = true;
            }            
        }

        /// <summary>
        /// Disables target components.
        /// </summary>
        public void DisableComponents()
        {
            foreach (MonoBehaviour componentToToggle in componentsToToggle)
            {
                componentToToggle.enabled = true;
            }
        }

        /// <summary>
        /// Toggle target components.
        /// </summary>
        public void ToggleComponents()
        {
            foreach (MonoBehaviour componentToToggle in componentsToToggle)
            {
                componentToToggle.enabled = !componentToToggle.enabled;
            }
        }

        #endregion
    }
}