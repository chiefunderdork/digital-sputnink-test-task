﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Scriptable object that holds configuration data for a <see cref="TransformableImage"/>.
    /// </summary>
    [CreateAssetMenu(fileName = "TransformableImageConfig", menuName = "Scriptable Objects/Configs/Transformable Image Config", order = 0)]
    public class TransformableImageConfig : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Unique image ID.
        /// </summary>
        /// <value>
        /// Gets the value of the int field id.
        /// </value>
        public int ID => id;

        /// <summary>
        /// Image name.
        /// </summary>
        /// <value>
        /// Gets the value of the string imageName.
        /// </value>
        public string ImageName => imageName;

        /// <summary>
        /// Prefab containing the transformable image.
        /// </summary>
        /// /// <remarks>
        /// For the purpose of the test task, we're using prefabs as images can have 3d objects attached to them.
        /// In a real projects, this could probably be replaced with some configuration files for each image.
        /// </remarks>
        /// <value>
        /// Gets the value of the game object field prefab.
        /// </value>
        public GameObject Prefab => prefab;

        /// <summary>
        /// Sprite that represents the image in the gallery.
        /// </summary>
        /// <value>
        /// Gets the value of the sprite field gallerySprite.
        /// </value>
        public Sprite GallerySprite => gallerySprite;

        #endregion

        #region Fields

        /// <summary>
        /// Unique image ID.
        /// </summary>
        [Tooltip("Unique image ID.")]
        [SerializeField]
        private int id = 0;

        /// <summary>
        /// Image name.
        /// </summary>
        [Tooltip("Image name.")]
        [SerializeField]
        private string imageName = "";

        /// <summary>
        /// Prefab containing the transformable image.
        /// </summary>
        /// <remarks>
        /// For the purpose of the test task, we're using prefabs as images can have 3d objects attached to them.
        /// In a real projects, this could probably be replaced with some configuration files for each image.
        /// </remarks>
        [Tooltip("Prefab containing the transformable image.")]
        [SerializeField]
        private GameObject prefab = null;

        /// <summary>
        /// Sprite that represents the image in the gallery.
        /// </summary>
        [Tooltip("Sprite that represents the image in the gallery.")]
        [SerializeField]
        private Sprite gallerySprite = null;

        #endregion
    }
}