﻿using TMPro;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles updating and showing debug info on transformable images when debug mode is enabled.
    /// </summary>
    public class DebugInfoManager : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that displays the name debug text.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Component that displays the name debug text.")]
        [SerializeField]
        private TextMeshProUGUI nameText = null;

        /// <summary>
        /// Component that displays the name debug text.
        /// </summary>
        [Tooltip("Component that displays the position debug text.")]
        [SerializeField]
        private TextMeshProUGUI positionText = null;

        /// <summary>
        /// Component that displays the name debug text.
        /// </summary>
        [Tooltip("Component that displays the scale debug text.")]
        [SerializeField]
        private TextMeshProUGUI scaleText = null;

        /// <summary>
        /// Transformable image that this component is connected to.
        /// </summary>
        [Tooltip("Transformable image that this component is connected to.")]
        [SerializeField]
        private TransformableImage transformableImage = null;

        /// <summary>
        /// Component that draws the GUI elements used for debug info.
        /// </summary>
        [Tooltip("Component that draws the GUI elements used for debug info.")]
        [SerializeField]
        private Canvas debugInfoCanvas = null;

        /// <summary>
        /// Is this component currently enabled?
        /// </summary>
        private bool isEnabled = false;

        #endregion

        #region Constants

        /// <summary>
        /// String to use to show position debug info.
        /// </summary>
        private const string PositionString = "X: {0} Y: {1}";

        /// <summary>
        /// String to use to show scale debug info.
        /// </summary>
        private const string ScaleString = "Scale: {0}";

        #endregion

        #region Init

        private void Start()
        {
            nameText.text = transformableImage.Name;
        }

        #endregion

        #region Update

        /// <summary>
        /// Enables the debug info.
        /// </summary>
        public void Enable()
        {
            UpdateInfo();
            isEnabled = debugInfoCanvas.enabled = true;
        }

        /// <summary>
        /// Disables the debug info.
        /// </summary>
        public void Disable()
        {
            isEnabled = debugInfoCanvas.enabled = false;
        }

        /// <summary>
        /// Updates the debug information.
        /// </summary>
        private void UpdateInfo()
        {
            Vector2 currentPosition = transformableImage.transform.position;
            positionText.text = string.Format(PositionString, currentPosition.x.ToString("F2"), currentPosition.y.ToString("F2"));
            scaleText.text = string.Format(ScaleString, transformableImage.transform.localScale.x.ToString("F2"));
        }

        private void Update()
        {
            if (isEnabled)
            {
                UpdateInfo();
                transform.rotation = Quaternion.identity;                
                transform.localScale = Vector3.one / transformableImage.transform.localScale.x;
            }
        }

        #endregion
    }
}