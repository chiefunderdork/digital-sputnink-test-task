﻿using DigitalSputnikTestTask.ScriptableObjects;
using DigitalSputnikTestTask.Events;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles behavior of an image that can be added to the scene.
    /// </summary>
    public class TransformableImage : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        #region Properties

        /// <summary>
        /// Apps's main camera.
        /// </summary>
        public static Camera MainCamera { get; set; }

        /// <summary>
        /// Image that the character is currently dragging.
        /// </summary>
        public static TransformableImage DraggedImage { get; set; } = null;

        /// <summary>
        /// RectTransform component on the slide out menu.
        /// </summary>
        public static RectTransform SlideOutMenuRectTransform { get; set; }

        /// <summary>
        /// Image used as an interaction hint for when player drags the image or its handles.
        /// </summary>
        public static Image InteractionHint { get; set; }

        /// <summary>
        /// Transform that markes the edge of the slide out menu.
        /// </summary>
        public static Transform SlideOutPanelEdgeMarker { get; set; }

        /// <summary>
        /// Unique image ID.
        /// </summary>
        public int ID => transformableImageConfig.ID;

        /// <summary>
        /// Image name.
        /// </summary>
        public string Name => transformableImageConfig.ImageName;

        /// <summary>
        /// Prefab of the transformable image.
        /// </summary>
        public GameObject Prefab => transformableImageConfig.Prefab;

        /// <summary>
        /// Is this image currently being destroyed?
        /// </summary>
        public bool IsBeingDestroyed { get; set; } = false;

        /// <summary>
        /// Handles used to scale and rotate the image.
        /// </summary>
        /// <value>
        /// Gets the value of the field transformableImageHandles.
        /// </value>
        public TransformableImageHandle[] TransformableImageHandles => transformableImageHandles;

        #endregion

        #region Fields

        /// <summary>
        /// Handles used to scale and rotate the image.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Handles used to scale and rotate the image.")]
        [SerializeField]
        private TransformableImageHandle[] transformableImageHandles = null;

        /// <summary>
        /// 3D models parented to this image.
        /// </summary>
        [Tooltip("3D models parented to this image.")]
        [SerializeField]
        private Transform[] connectedModels = null;

        /// <summary>
        /// Sprite renderer that renders the image.
        /// </summary>
        [Tooltip("Sprite renderer that renders the image.")]
        [SerializeField]
        private SpriteRenderer imageRenderer = null;

        /// <summary>
        /// Sprite renderer used to highlight thew image when it's hovered over the trash can.
        /// </summary>
        [Tooltip("Sprite renderer used to highlight thew image when it's hovered over the trash can.")]
        [SerializeField]
        private SpriteRenderer deletionHighlightRenderer = null;

        /// <summary>
        /// Component that handles showing and updating image debug info.
        /// </summary>
        [Tooltip("Component that handles showing and updating image debug info.")]
        [SerializeField]
        private DebugInfoManager debugInfoManager = null;

        /// <summary>
        /// Scriptable object that holds configuration data for a <see cref="TransformableImage"/>.
        /// </summary>
        [Space]
        [Header("Assets")]
        [Space]
        [Tooltip("Scriptable object that holds configuration data for a TransformableImage.")]
        [SerializeField]
        private TransformableImageConfig transformableImageConfig = null;

        /// <summary>
        /// Scriptable object that holds the position of the player's input on current frame.
        /// </summary>
        [Tooltip("Scriptable object that holds the position of the player's input on current frame.")]
        [SerializeField]
        private Vector2Variable currentInputPosition = null;

        /// <summary>
        /// Scriptable object that holds the position of the player's input on previous frame.
        /// </summary>
        [Tooltip("Scriptable object that holds the position of the player's input on previous frame.")]
        [SerializeField]
        private Vector2Variable previousInputPosition = null;

        /// <summary>
        /// Event that is raised when user starts dragging this image.
        /// </summary>
        [Space]
        [Header("Events")]
        [Space]
        [Tooltip("Event that is raised when user starts dragging this image.")]
        [SerializeField]
        private GameEvent onStartedDraggingImage = null;

        /// <summary>
        /// Event that is raised when user stops dragging this image.
        /// </summary>
        [Tooltip("Event that is raised when user stops dragging this image.")]
        [SerializeField]
        private GameEvent onStoppedDraggingImage = null;

        /// <summary>
        /// Is image currently being dragged?
        /// </summary>
        private bool isBeingDragged = false;

        /// <summary>
        /// Is the image currently hovered over a trash can?
        /// </summary>
        private bool isDraggedOverTrashCan = false;

        /// <summary>
        /// Position of the image when user started dragging.
        /// </summary>
        private Vector2 dragStartPosition;

        /// <summary>
        /// Length of image's diagonal when scale is one.
        /// </summary>
        private float scaleOneDiagonalLength;

        #endregion

        #region Init

        private void Start()
        {
            scaleOneDiagonalLength = Vector3.Distance(transformableImageHandles[0].transform.position, transformableImageHandles[2].transform.position) 
                                     / transform.localScale.x;           
        }

        #endregion

        #region Transformations

        #region Position

        /// <summary>
        /// Event system callback for when user starts dragging this object.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (SaveGalleryMenu.IsEnabled || LoadGalleryMenu.IsEnabled)
            {
                return;
            }

            foreach (TransformableImageHandle transformableImageHandle in transformableImageHandles)
            {
                if (transformableImageHandle.HandleCollider.OverlapPoint(MainCamera.ScreenToWorldPoint(eventData.position))) 
                {
                    return;
                }
            }

            InteractionHint.transform.position = MainCamera.WorldToScreenPoint(transform.position);
            InteractionHint.enabled = true;
            onStartedDraggingImage.Raise();           
            dragStartPosition = transform.position;
            ImageManager.SortUp(this);
            isBeingDragged = true;
            DraggedImage = this;
        }

        /// <summary>
        /// Event system callback for when user drags this object during current frame.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnDrag(PointerEventData eventData)
        {
            if (!isBeingDragged)
            {
                return;
            }

            if (SaveGalleryMenu.IsEnabled || LoadGalleryMenu.IsEnabled)
            {
                return;
            }

            foreach (TransformableImageHandle transformableImageHandle in transformableImageHandles)
            {
                if (HandleHasReachedMenuPanel(transformableImageHandle))
                {
                    Vector3 slideOutPanelEdgeMarkerPosition = MainCamera.ScreenToWorldPoint(SlideOutPanelEdgeMarker.position);

                    float penetration = Mathf.Abs(transformableImageHandle.transform.position.x - slideOutPanelEdgeMarkerPosition.x);

                    transform.position += Vector3.right * (penetration + 0.01f);
                    return;
                }
            }

            InteractionHint.transform.position = MainCamera.WorldToScreenPoint(transform.position);

            Vector2 previousScreenPosition = eventData.position - eventData.delta;
            Vector2 previousWorldPosition = MainCamera.ScreenToWorldPoint(previousScreenPosition);
            Vector2 newWorldPosition = MainCamera.ScreenToWorldPoint(eventData.position);

            transform.position += (Vector3)eventData.delta.normalized * Vector3.Distance(previousWorldPosition, newWorldPosition);
        }

        /// <summary>
        /// Event system callback for when user stops dragging this object.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnEndDrag(PointerEventData eventData)
        {
            if (isDraggedOverTrashCan)
            {
                InteractionHint.enabled = false;
                UndoRedoStack.AddAction(new MoveUndoRedoAction(dragStartPosition, transform.position, UndoRedoActionType.Move, transform));

                onStoppedDraggingImage.Raise();
                StopAllCoroutines();
                ImageManager.DestroyImage(this, addToUndoRedoStack: true);
                return;
            }

            onStoppedDraggingImage.Raise();
            isBeingDragged = false;

            UndoRedoStack.AddAction(new MoveUndoRedoAction(dragStartPosition, transform.position, UndoRedoActionType.Move, transform));

            DraggedImage = null;

            InteractionHint.enabled = false;
        }

        /// <summary>
        /// Starts the <see cref="DragAfterInstantiatingCoroutine"/>.
        /// </summary>
        public void StartDragAfterInstantiatingCoroutine() => _ = StartCoroutine(DragAfterInstantiatingCoroutine());

        /// <summary>
        /// Coroutine that handles dragging the image immediately after it has been instantiated in the scene.
        /// </summary>
        /// <returns>
        /// An IEnumerator.
        /// </returns>
        private IEnumerator DragAfterInstantiatingCoroutine()
        {
            onStartedDraggingImage.Raise();

            while (true)
            {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN

                if (Input.GetMouseButtonUp(0))
                {
                    onStoppedDraggingImage.Raise();

                    if (isDraggedOverTrashCan)
                    {
                        StopAllCoroutines();
                        Destroy(gameObject);
                    }

                    UndoRedoStack.AddAction(new CreateUndoRedoAction(transform.position, this, UndoRedoActionType.Create, transform));

                    yield break;
                }

#else
                if (Input.touchCount == 0)
                {
                    onStoppedDraggingImage.Raise();

                    if (isDraggedOverTrashCan)
                    {
                        StopAllCoroutines();
                        Destroy(gameObject);
                    }

                    UndoRedoStack.AddAction(new CreateUndoRedoAction(transform.position, this, UndoRedoActionType.Create, transform));

                    yield break;
                }
#endif                
                Vector3 oldWorldPosition = MainCamera.ScreenToWorldPoint(previousInputPosition.Value);
                Vector3 newWorldPosition = MainCamera.ScreenToWorldPoint(currentInputPosition.Value);

                Vector3 worldDelta = newWorldPosition - oldWorldPosition;

                transform.position += worldDelta;                    
                
                yield return null;
            }            
        }


        /// <summary>
        /// Returns True if the passed iamge handle has reached menu panel, False otherwise.
        /// </summary>
        /// <param name="transformableImageHandle">Handle to check.</param>
        /// <returns>
        /// A boolean.
        /// </returns>
        public bool HandleHasReachedMenuPanel(TransformableImageHandle transformableImageHandle)
        {
            Vector2 handleScreenPosition = MainCamera.WorldToScreenPoint(transformableImageHandle.transform.position);

            if (RectTransformUtility.RectangleContainsScreenPoint(SlideOutMenuRectTransform, handleScreenPosition))
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Rotation

        /// <summary>
        /// Rotates the image by specified amount of degrees.
        /// </summary>
        /// <param name="rotationAngle">Rotation amount.</param>
        public void Rotate(float rotation) => transform.Rotate(Vector3.forward, rotation);

        #endregion

        #region Scale

        /// <summary>
        /// Scales the image to the new diagonal length.
        /// </summary>
        /// <param name="newDiagonalLength">New length of the image diagonal.</param>
        public void Scale(float newDiagonalLength)
        {
            transform.localScale = Vector3.one * (newDiagonalLength / scaleOneDiagonalLength);
        }

        #endregion

        #endregion

        #region Sorting

        /// <summary>
        /// Sets the sorting order on the <see cref="imageRenderer"/>.
        /// </summary>
        /// <param name="order"></param>
        public void SetSortingLayerOrder(int order) => imageRenderer.sortingOrder = order;

        #endregion

        #region Trash can

        /// <summary>
        /// Toggles the sprite renderer that draws the deletion highlight on or off/
        /// </summary>
        /// <param name="toggleOn">Should the renderer be toggle on?</param>
        public void ToggleDeletionHighlight(bool toggleOn)
        {
            if (DraggedImage != this)
            {
                return;
            }

            deletionHighlightRenderer.enabled = toggleOn;
        }

        /// <summary>
        /// Sets the value of the <see cref="isDraggedOverTrashCan"/> flag.
        /// </summary>
        /// <param name="value">New flag value.</param>
        public void SetIsDraggedOverTrashCanFlag(bool value) => isDraggedOverTrashCan = value;

        #endregion

        #region Debug

        /// <summary>
        /// Enables the <see cref="debugInfoManager"/>.
        /// </summary>
        public void ShowDebugInfo() => debugInfoManager.Enable();

        /// <summary>
        /// Disables the <see cref="debugInfoManager"/>.
        /// </summary>
        public void HideDebugInfo() => debugInfoManager.Disable();

        #endregion
    }
}