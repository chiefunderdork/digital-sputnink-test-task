﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Object that represents a <see cref="TransformableImage"/> in the add picture menu.
    /// </summary>
    public class AddPictureImage : MonoBehaviour, IBeginDragHandler, IDragHandler
    {
        #region Fields

        /// <summary>
        /// GUI component that draws the gallery image.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("GUI component that draws the gallery image.")]
        [SerializeField]
        private Image image = null;        

        /// <summary>
        /// Image's box collider.
        /// </summary>
        [Tooltip("Image's box collider.")]
        [SerializeField]
        private BoxCollider2D imageCollider = null;

        /// <summary>
        /// Object that serves as a mock of a transformable image when dragging from the add picture menu to the gallery.
        /// </summary>
        [Tooltip("Object that serves as a mock of a transformable image when dragging from the add picture menu to the gallery.")]
        [SerializeField]
        private TransformableImageMock transformableImageMock = null;

        /// <summary>
        /// Scriptable object that holds configuration data for a <see cref="TransformableImage"/>.
        /// </summary>
        [Space]
        [Header("Assets")]
        [Space]
        [Tooltip("Scriptable object that holds configuration data for a TransformableImage.")]
        [SerializeField]
        private TransformableImageConfig transformableImageConfig = null;        

        #endregion

        #region Toggle

        /// <summary>
        /// Toggles the image.
        /// </summary>
        /// <param name="toggleOn">Should the image be toggled on?</param>
        public void Toggle(bool toggleOn) => image.enabled = imageCollider.enabled = toggleOn;

        #endregion

        #region Drag

        public void OnBeginDrag(PointerEventData eventData)
        {
            transformableImageMock.Enable(transformableImageConfig);
        }

        public void OnDrag(PointerEventData eventData)
        {         
        }

        #endregion

#if UNITY_EDITOR

        #region Editor

        private void OnValidate()
        {
            if (UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }

            image.sprite = transformableImageConfig.GallerySprite;
        }

        #endregion
#endif
    }
}