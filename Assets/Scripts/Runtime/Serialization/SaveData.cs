﻿using System.Collections.Generic;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Stores save data for all galleries.
    /// </summary>
    [System.Serializable]
    public class SaveData
    {       
        #region Properties

        /// <summary>
        /// Array of objects containig save data about image galleries.
        /// </summary>
        /// <value>
        /// Gets the value of the field galerySaveData.
        /// </value>
        public List<GallerySaveData> GalerySaveData => gallerySaveData;

        #endregion

        #region Fields

        /// <summary>
        /// Array of objects containig save data about image galleries.
        /// </summary>
        [SerializeField]
        private List<GallerySaveData> gallerySaveData;

        #endregion

        #region Constants

        /// <summary>
        /// Maximum number of saved images galleries.
        /// </summary>
        private const int MaxNumberOfSavedGalleries = 10;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public SaveData()
        {
            gallerySaveData = new List<GallerySaveData>();
        }

        #endregion

        #region Data

        /// <summary>
        /// Adds save data for a gallery to save data. Checks if data with the same path already exists and overwrites it if it does.
        /// </summary>
        /// <param name="dataToAdd">Data to add.</param>
        public void AddGallerySaveData(GallerySaveData dataToAdd)
        {
            if (gallerySaveData.Count == 0)
            {
                gallerySaveData.Add(dataToAdd);
                return;
            }

            for (int i = gallerySaveData.Count - 1; i >= 0; i--)
            {
                GallerySaveData currentData = gallerySaveData[i];

                if (currentData.GalleryName.Equals(dataToAdd.GalleryName))
                {
                    gallerySaveData.Remove(currentData);
                    gallerySaveData.Add(dataToAdd);
                    return;
                }
            }

            gallerySaveData.Add(dataToAdd);
        }

        #endregion
    }
}