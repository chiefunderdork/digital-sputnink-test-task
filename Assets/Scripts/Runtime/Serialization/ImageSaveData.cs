﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Stores save data about an image placed in the gallery.
    /// </summary>
    [System.Serializable]
    public struct ImageSaveData
    {        
        #region Properties

        /// <summary>
        /// Image's unique ID.
        /// </summary>
        /// <value>
        /// Gets the value of the int field imageID.
        /// </value>
        public int ImageID => imageID;

        /// <summary>
        /// X component of image position.
        /// </summary>
        /// <value>
        /// Gets the value of the float field positionX.
        /// </value>
        public float PositionX => positionX;

        /// <summary>
        /// Y component of image position.
        /// </summary>
        /// <value>
        /// Gets the value of the float field positionY.
        /// </value>
        public float PositionY => positionY;

        /// <summary>
        /// Image rotation.
        /// </summary>
        /// <value>
        /// Gets the value of the float field rotation.
        /// </value>
        public float Rotation => rotation;

        /// <summary>
        /// Image scale.
        /// </summary>
        /// <value>
        /// Gets the value of the float field scale.
        /// </value>
        public float Scale => scale;

        #endregion

        #region Fields

        /// <summary>
        /// Image's unique ID.
        /// </summary>
        [SerializeField]
        private int imageID;

        /// <summary>
        /// X component of image position.
        /// </summary>
        [SerializeField]
        private float positionX;

        /// <summary>
        /// Y component of image position.
        /// </summary>
        [SerializeField]
        private float positionY;

        /// <summary>
        /// Image rotation.
        /// </summary>
        [SerializeField]
        private float rotation;

        /// <summary>
        /// Image scale.
        /// </summary>
        [SerializeField]
        private float scale;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="imageID">Image's unique ID.</param>
        /// <param name="positionX">X component of image position.</param>
        /// <param name="positionY">Y component of image position.</param>
        /// <param name="rotation">Image rotation.</param>
        /// <param name="scale">Image scale.</param>
        public ImageSaveData(int imageID, float positionX, float positionY, float rotation, float scale)
        {
            this.imageID = imageID;
            this.positionX = positionX;
            this.positionY = positionY;
            this.rotation = rotation;
            this.scale = scale;
        }

        #endregion
    }
}