﻿using System;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Stores save data about an image gallery.
    /// </summary>
    [System.Serializable]
    public class GallerySaveData
    {
        #region Properties

        /// <summary>
        /// Gallery name.
        /// </summary>
        /// <value>
        /// Gets the value of the string field galleryName.
        /// </value>
        public string GalleryName => galleryName;

        /// <summary>
        /// Object containing data about current date and time.
        /// </summary>
        /// <value>
        /// Gets the value of the field currentDateTime;
        /// </value>
        public DateTime CurrentDataTime => currentDateTime;

        /// <summary>
        /// Objects containing saved image data for each image in the gallery.
        /// </summary>
        public ImageSaveData[] SavedImageData => savedImageData;

        #endregion

        #region Fields

        /// <summary>
        /// Gallery name.
        /// </summary>
        [SerializeField]
        private string galleryName;

        /// <summary>
        /// Object containing data about current date and time.
        /// </summary>
        [SerializeField]
        private DateTime currentDateTime;

        /// <summary>
        /// Objects containing saved image data for each image in the gallery.
        /// </summary>
        [SerializeField]
        private ImageSaveData[] savedImageData;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="galleryName">Gallery name.</param>
        /// <param name="currentDateTime">Object containing data about current date and time.</param>
        /// <param name="savedImageData">Objects containing saved image data for each image in the gallery.</param>
        public GallerySaveData(string galleryName, DateTime currentDateTime, ImageSaveData[] savedImageData)
        {
            this.galleryName = galleryName;
            this.currentDateTime = currentDateTime;
            this.savedImageData = savedImageData;
        }

        #endregion
    }
}