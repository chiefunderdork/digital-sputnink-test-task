﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    public class ScreenOrientationSetter : MonoBehaviour
    {
#if UNITY_ANDROID || UNITY_IOS

        private void Awake()
        {
            Screen.autorotateToLandscapeLeft = Screen.autorotateToLandscapeRight = true;
            Screen.autorotateToPortrait = Screen.autorotateToPortraitUpsideDown = false;
        }
#endif
    }
}