﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Base class for image actions that can be undone/redone.
    /// </summary>
    [System.Serializable]
    public abstract class UndoRedoAction
    {
        #region Properties

        /// <summary>
        /// Type of stored undo/redo action.
        /// </summary>
        public UndoRedoActionType UndoRedoActionType { get; }

        /// <summary>
        /// Transform of the image that the action has been performed on.
        /// </summary>
        public Transform ImageTransform { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="undoRedoActionType">Type of stored undo/redo action.</param>
        /// <param name="imageTransform">Transform of the target image.</param>
        public UndoRedoAction(UndoRedoActionType undoRedoActionType, Transform imageTransform)
        {
            UndoRedoActionType = undoRedoActionType;
            ImageTransform = imageTransform;
        }

        #endregion

        #region Abstract
        public abstract void Undo();
        public abstract void Redo();

        #endregion
    }
}