﻿using DigitalSputnikTestTask.Events;
using System.Collections.Generic;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Contains undo data for image transformations.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class UndoRedoStack
    {
        #region Properties

        /// <summary>
        /// Size of the undo stack.
        /// </summary>
        public static int SizeOfUndoStack => undoStack.Count;

        /// <summary>
        /// Size of the redo stack.
        /// </summary>
        public static int SizeOfRedoStack => redoStack.Count;

        /// <summary>
        /// Event that is raised when the undo stack is updated.
        /// </summary>
        public static GameEvent OnUndoStackUpdated { get; set; }

        /// <summary>
        /// Event that is raised when the redo stack is updated.
        /// </summary>
        public static GameEvent OnRedoStackUpdated { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// Stack containing undo operations.
        /// </summary>
        private static UndoStack undoStack;

        /// <summary>
        /// Stack containing redo operations.
        /// </summary>
        private static Stack<UndoRedoAction> redoStack;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        static UndoRedoStack() => Reset();

        #endregion

        #region Init

        /// <summary>
        /// Resets the stacks.
        /// </summary>
        private static void Reset()
        {
            undoStack = new UndoStack();
            redoStack = new Stack<UndoRedoAction>();
        }

        #endregion

        #region Stack operations

        /// <summary>
        /// Adds action to the undo stack, resets the redo stack.
        /// </summary>
        /// <param name="undoRedoAction">Action to add.</param>
        public static void AddAction(UndoRedoAction undoRedoAction)
        {
            undoStack.Push(undoRedoAction);
            redoStack.Clear();

            OnUndoStackUpdated.Raise();
            OnRedoStackUpdated.Raise();

            AddActionsWithSameImageToCreateAndDestroyActions();
            //Debug.LogFormat("Undo count: {0}. Redo count: {1}", undoStack.Count, redoStack.Count);
        }

        /// <summary>
        /// Gets the latest action in the undo stack and performs the undo.
        /// </summary>
        public static void Undo()
        {
            if (undoStack.Count > 0)
            {
                try
                {
                    UndoRedoAction actionToUndo = undoStack.Pop();
                    actionToUndo.Undo();
                    redoStack.Push(actionToUndo);
                }
                catch (System.NullReferenceException)
                {
                    ResetStacks();
                }                

                OnUndoStackUpdated.Raise();
                OnRedoStackUpdated.Raise();

                AddActionsWithSameImageToCreateAndDestroyActions();
            }            

            //Debug.LogFormat("Undo count: {0}. Redo count: {1}", undoStack.Count, redoStack.Count);
        }

        /// <summary>
        /// Gets the latest action in the redo stach and performs the redo.
        /// </summary>
        public static void Redo()
        {
            if (redoStack.Count > 0)
            {
                try
                {
                    UndoRedoAction actionToRedo = redoStack.Pop();
                    actionToRedo.Redo();
                    undoStack.Push(actionToRedo);
                }
                catch (System.NullReferenceException)
                {
                    ResetStacks();
                }                

                OnUndoStackUpdated.Raise();
                OnRedoStackUpdated.Raise();

                AddActionsWithSameImageToCreateAndDestroyActions();
            }

            //Debug.LogFormat("Undo count: {0}. Redo count: {1}", undoStack.Count, redoStack.Count);
        }

        /// <summary>
        /// Resets the undo and redo stacks.
        /// </summary>
        public static void ResetStacks()
        {
            undoStack.Clear();
            redoStack.Clear();

            OnUndoStackUpdated.Raise();
            OnRedoStackUpdated.Raise();
        }

        /// <summary>
        /// Gets a list of undo/redo actions that are connected to the passed image.
        /// </summary>
        /// <param name="transformableImage">Transformable image to check.</param>
        /// <returns>
        /// A list of <see cref="UndoRedoAction"/>.
        /// </returns>
        public static List<UndoRedoAction> GetActionsWithTheSameImage(UndoRedoAction targetAction, TransformableImage transformableImage)
        {
            if (transformableImage == null)
            {
                Debug.LogError("Transformable image is null!");
            }            

            var actionList = new List<UndoRedoAction>();

            foreach (UndoRedoAction undoAction in undoStack)
            {
                if (undoAction.ImageTransform == null)
                {
                    continue;
                }

                if (undoAction.ImageTransform == transformableImage.transform && undoAction != targetAction)
                {
                    actionList.Add(undoAction);
                }
            }

            foreach (UndoRedoAction redoAction in redoStack)
            {
                if (redoAction.ImageTransform == null)
                {
                    continue;
                }

                if (redoAction.ImageTransform == transformableImage.transform && redoAction != targetAction)
                {
                    actionList.Add(redoAction);
                }
            }

            return actionList;
        }

        /// <summary>
        /// Adds actions with the same image to create and destroy actions.
        /// </summary>
        private static void AddActionsWithSameImageToCreateAndDestroyActions()
        {
            foreach (UndoRedoAction undoAction in undoStack)
            {
                switch (undoAction.UndoRedoActionType)
                { 
                    case UndoRedoActionType.Create:
                        CreateUndoRedoAction createUndoRedoAction = (CreateUndoRedoAction)undoAction;
                        if (createUndoRedoAction.TransformableImage == null)
                        {
                            continue;
                        }
                        createUndoRedoAction.ActionsWithTheSameImage = GetActionsWithTheSameImage(undoAction, createUndoRedoAction.TransformableImage);
                        break;
                    case UndoRedoActionType.Destroy:
                        DestroyUndoRedoAction destroyUndoRedoAction = (DestroyUndoRedoAction)undoAction;
                        if (destroyUndoRedoAction.TransformableImage == null)
                        {
                            continue;
                        }
                        destroyUndoRedoAction.ActionsWithTheSameImage = GetActionsWithTheSameImage(undoAction, destroyUndoRedoAction.TransformableImage);
                        break;
                    default:
                        break;
                }
            }

            foreach (UndoRedoAction redoAction in redoStack)
            {
                switch (redoAction.UndoRedoActionType)
                {
                    case UndoRedoActionType.Create:
                        CreateUndoRedoAction createUndoRedoAction = (CreateUndoRedoAction)redoAction;
                        if (createUndoRedoAction.TransformableImage == null)
                        {
                            continue;
                        }
                        createUndoRedoAction.ActionsWithTheSameImage = GetActionsWithTheSameImage(redoAction, createUndoRedoAction.TransformableImage);
                        break;
                    case UndoRedoActionType.Destroy:
                        DestroyUndoRedoAction destroyUndoRedoAction = (DestroyUndoRedoAction)redoAction;
                        if (destroyUndoRedoAction == null)
                        {
                            continue;
                        }
                        destroyUndoRedoAction.ActionsWithTheSameImage = GetActionsWithTheSameImage(redoAction, destroyUndoRedoAction.TransformableImage);
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion
    }
}

