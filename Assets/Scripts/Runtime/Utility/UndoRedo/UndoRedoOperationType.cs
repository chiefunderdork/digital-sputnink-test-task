﻿namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Type of an undo/redo operation.
    /// </summary>
    public enum UndoRedoOperationType
    {
        Undo,
        Redo
    }
}