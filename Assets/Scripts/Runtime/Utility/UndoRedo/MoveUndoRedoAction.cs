﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Move image action that can be undone/redone.
    /// </summary>
    public class MoveUndoRedoAction : UndoRedoAction
    {
        #region Fields

        /// <summary>
        /// Image position when it started moving.
        /// </summary>
        private Vector3 startPosition;

        /// <summary>
        /// Image position when it stopped moving.
        /// </summary>
        private Vector3 endPosition;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="startPosition">Image position when it started moving.</param>
        /// <param name="endPosition">Image position when it stopped moving.</param>
        /// <param name="undoRedoActionType">Type of this action.</param>
        /// <param name="imageTransform">Transform of the target image.</param>
        public MoveUndoRedoAction(Vector3 startPosition, Vector3 endPosition, UndoRedoActionType undoRedoActionType,
                                  Transform imageTransform) : base(undoRedoActionType, imageTransform)
        {
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        }

        #endregion

        #region Undo/redo 

        /// <summary>
        /// Performs the undo operation on target image's position.
        /// </summary>
        public override void Undo()
        {
            //Debug.Log("Undoing move");
            ImageTransform.position = startPosition;
        }

        /// <summary>
        /// Performs the redo operation on target image's position.
        /// </summary>
        public override void Redo()
        {
            //Debug.Log("Redoing move");
            ImageTransform.position = endPosition;
        }

        #endregion
    }
}