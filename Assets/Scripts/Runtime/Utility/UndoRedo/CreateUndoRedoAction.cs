﻿using System.Collections.Generic;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Image create action that can be undo/redone.
    /// </summary>
    public class CreateUndoRedoAction : UndoRedoAction
    {
        #region Properties

        /// <summary>
        /// List of actions that were performed on the same image as the destroy action.
        /// </summary>
        public List<UndoRedoAction> ActionsWithTheSameImage { get; set; }

        /// <summary>
        /// Transformable image that was created.
        /// </summary>
        public TransformableImage TransformableImage { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// Image position when it was created.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Prefab of the attached transformable image.
        /// </summary>
        private GameObject transformableImagePrefab;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="position">Image position when it was created.</param>
        /// <param name="transformableImage">Transformable image that was created..</param>
        /// <param name="undoRedoActionType">Type of this action.</param>
        /// <param name="imageTransform">Transform of the target image.</param>
        public CreateUndoRedoAction(Vector3 position, TransformableImage transformableImage, 
                                    UndoRedoActionType undoRedoActionType, Transform imageTransform) : base(undoRedoActionType, imageTransform)
        {
            this.position = position;
            TransformableImage = transformableImage;
            transformableImagePrefab = transformableImage.Prefab;

            ActionsWithTheSameImage = UndoRedoStack.GetActionsWithTheSameImage(this, transformableImage);
        }

        #endregion

        #region Undo/redo

        /// <summary>
        /// Performs the undo operation on target image's create action.
        /// </summary>
        public override void Undo()
        {
            //Debug.Log("Undoing create");
            ImageManager.DestroyImage(TransformableImage, addToUndoRedoStack: false);
        }

        /// <summary>
        /// Performs the redo operation on target image's create action.
        /// </summary>
        public override void Redo()
        {
            //Debug.Log("Redoing create");
            TransformableImage = ImageManager.SpawnTransformableImage(transformableImagePrefab, position, instantiatedFromUndoRedoStack: true);

            foreach (UndoRedoAction undoRedoAction in ActionsWithTheSameImage)
            {
                if (undoRedoAction == null)
                {
                    continue;
                }

                switch (undoRedoAction.UndoRedoActionType)
                {
                    case UndoRedoActionType.Create:
                        ((CreateUndoRedoAction)undoRedoAction).TransformableImage = TransformableImage;
                        break;
                    case UndoRedoActionType.Destroy:
                        ((DestroyUndoRedoAction)undoRedoAction).TransformableImage = TransformableImage;
                        break;
                    default:
                        break;

                }

                undoRedoAction.ImageTransform = TransformableImage.transform;  
            }
        }       

        #endregion
    }
}