﻿using System.Collections.Generic;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Image destruction action that can be undone/redone.
    /// </summary>
    public class DestroyUndoRedoAction : UndoRedoAction
    {
        #region Properties

        /// <summary>
        /// List of actions that were performed on the same image as the destroy action.
        /// </summary>
        public List<UndoRedoAction> ActionsWithTheSameImage { get; set; }

        /// <summary>
        /// Transformable image that was created.
        /// </summary>
        public TransformableImage TransformableImage { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// Image position when it was created.
        /// </summary>
        private Vector3 position;

        /// <summary>
        /// Image rotation.
        /// </summary>
        private Quaternion rotation;

        /// <summary>
        /// Image scale.
        /// </summary>
        private float scale;

        /// <summary>
        /// Prefab of the attached transformable image.
        /// </summary>
        private GameObject transformableImagePrefab;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="transformableImage">Transformable image that was created.</param>
        /// <param name="position">Image position when it was created.</param>
        /// <param name="rotation">Image rotation.</param>
        /// <param name="scale">Image scale.</param>
        /// <param name="undoRedoActionType">Type of this action.</param>
        /// <param name="imageTransform">Transform of the target image.</param>
        public DestroyUndoRedoAction(TransformableImage transformableImage, Vector3 position, Quaternion rotation, float scale,
                                    UndoRedoActionType undoRedoActionType, Transform imageTransform) : base(undoRedoActionType, imageTransform)
        {
            TransformableImage = transformableImage;
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
            transformableImagePrefab = transformableImage.Prefab;

            ActionsWithTheSameImage = UndoRedoStack.GetActionsWithTheSameImage(this, transformableImage);
        }

        #endregion

        #region Undo/redo

        /// <summary>
        /// Performs the undo operation on target image's create action.
        /// </summary>
        public override void Undo()
        {
            //Debug.Log("Undoing destroy");
            TransformableImage = ImageManager.SpawnTransformableImage(transformableImagePrefab, position, rotation, scale, instantiatedFromUndoRedoStack: true);

            foreach (UndoRedoAction undoRedoAction in ActionsWithTheSameImage)
            {
                if (undoRedoAction == null)
                {
                    continue;
                }

                switch (undoRedoAction.UndoRedoActionType)
                {
                    case UndoRedoActionType.Create:
                        ((CreateUndoRedoAction)undoRedoAction).TransformableImage = TransformableImage;
                        break;
                    case UndoRedoActionType.Destroy:
                        ((DestroyUndoRedoAction)undoRedoAction).TransformableImage = TransformableImage;
                        break;
                    default:
                        break;

                }

                undoRedoAction.ImageTransform = TransformableImage.transform;  
            }
        }

        /// <summary>
        /// Performs the redo operation on target image's create action.
        /// </summary>
        public override void Redo()
        {
            //Debug.Log("Redoing destroy");

            Debug.Log("Destroy action is destroying with transformable image: " + TransformableImage);

            ImageManager.DestroyImage(TransformableImage, addToUndoRedoStack: false);
        }

        #endregion
    }
}