﻿using System.Collections.Generic;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// A stack-like object that contains undo data.
    /// </summary>
    public class UndoStack : LinkedList<UndoRedoAction>
    {
        #region Constants

        /// <summary>
        /// Maximum number of undo operations that can be stored in memory at the same time.
        /// </summary>
        private const int MaxUndoOperations = 15;

        #endregion

        #region Stack

        /// <summary>
        /// Gets the first element of the stack.
        /// </summary>
        /// <returns>
        /// <see cref="UndoRedoAction"/>.
        /// </returns>
        public UndoRedoAction Pop()
        {
            UndoRedoAction first = First?.Value;

            if (first != null)
            {
                RemoveFirst();
            }
            return first;
        }

        /// <summary>
        /// Pushes an object to the stack. Removes the oldest element if stack capacity is exceeded.
        /// </summary>
        /// <param name="undoRedoData">Undo/redo data to push.</param>
        public void Push(UndoRedoAction undoRedoData)
        {
            if (Count == MaxUndoOperations)
            {
                RemoveLast();
            }

            AddFirst(undoRedoData);            
        }

        #endregion
    }
}