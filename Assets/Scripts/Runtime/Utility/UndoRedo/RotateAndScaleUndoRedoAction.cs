﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Rotate image action that can be undone/redone.
    /// </summary>
    public class RotateAndScaleUndoRedoAction : UndoRedoAction
    {
        #region Fields

        /// <summary>
        /// Image rotation when it started rotating.
        /// </summary>
        private Quaternion startRotation;

        /// <summary>
        /// Image rotation when it stopped rotating.
        /// </summary>
        private Quaternion endRotation;

        /// <summary>
        /// Image scale when it started scaling.
        /// </summary>
        private Vector3 startScale;

        /// <summary>
        /// Image scale when it stopped scaling.
        /// </summary>
        private Vector3 endScale;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="startRotation">Image rotation when it started rotating.</param>
        /// <param name="endRotation">Image rotation when it stopped rotating.</param>
        /// <param name="startScale">Image scale when it started scaling.</param>
        /// <param name="endScale">Image scale when it stopped scaling.</param>
        /// <param name="undoRedoActionType">Type of this action.</param>
        /// <param name="imageTransform">Transform of the target image.</param>
        public RotateAndScaleUndoRedoAction(Quaternion startRotation, Quaternion endRotation, Vector3 startScale, Vector3 endScale, 
                                            UndoRedoActionType undoRedoActionType, Transform imageTransform) : base(undoRedoActionType, imageTransform)
        {
            this.startRotation = startRotation;
            this.endRotation = endRotation;
            this.startScale = startScale;
            this.endScale = endScale;
        }

        #endregion

        #region Undo/redo 

        /// <summary>
        /// Performs the undo operation on target image's scale.
        /// </summary>
        public override void Undo()
        {
            ImageTransform.rotation = startRotation;
            ImageTransform.localScale = startScale;
        }

        /// <summary>
        /// Performs the redo operation on target image's scale.
        /// </summary>
        public override void Redo()
        {
            ImageTransform.rotation = endRotation;
            ImageTransform.localScale = endScale;
        }

        #endregion
    }
}