﻿namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Type of an undo/redo operation on an image.
    /// </summary>
    public enum UndoRedoActionType
    {
        Move,
        RotateAndScale,        
        Create,
        Destroy
    }
}