﻿using DigitalSputnikTestTask.ScriptableObjects;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles player's input.
    /// </summary>
    public class InputManager : MonoBehaviour
    {
        #region Fields
        
        /// <summary>
        /// Scriptable object that holds the position of the player's input on current frame.
        /// </summary>
        [Space]
        [Header("Assets")]
        [Space]
        [Tooltip("Scriptable object that holds the position of the player's input on current frame.")]
        [SerializeField]
        private Vector2Variable currentInputPosition = null;

        /// <summary>
        /// Scriptable object that holds the position of the player's input on previous frame.
        /// </summary>
        [Tooltip("Scriptable object that holds the position of the player's input on previous frame.")]
        [SerializeField]
        private Vector2Variable previousInputPosition = null;

        /// <summary>
        /// Vector to set on the <see cref="currentInputPosition"/> SO when there is no valid input from the player.
        /// </summary>
        private Vector2 noInputVector = Vector2.down;

        #endregion

        #region Init

        private void Start()
        {
            currentInputPosition.Value = previousInputPosition.Value = noInputVector;
        }

        #endregion

        #region Input

        private void Update()
        {

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            previousInputPosition.Value = currentInputPosition.Value;
            currentInputPosition.Value = Input.mousePosition;
#else
            previousInputPosition.Value = currentInputPosition.Value;
#if UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount > 0)
            {
                currentInputPosition.Value = Input.GetTouch(0).position;
            }
            else
            {
                currentInputPosition.Value = noInputVector;
            }
#else
            currentInputPosition.Value = Input.mousePosition;
#endif

#endif
        }

#endregion
    }
}