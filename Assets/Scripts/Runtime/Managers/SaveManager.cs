﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles saving/loading image galleries.
    /// </summary>
    public static class SaveManager
    {
        #region Properties

        /// <summary>
        /// Latest version of save data loaded from disk.
        /// </summary>
        public static SaveData SaveData { get; set; }

        #endregion

        #region Constants

        /// <summary>
        /// Path to the file that stores image gallery data.
        /// </summary>
        private static readonly string saveFilePath = Application.persistentDataPath + "/gallerySaveData.dat";    

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        static SaveManager() => _ = LoadGalleryData();

        #endregion

        #region Save

        /// <summary>
        /// Saves a gallery.
        /// </summary>
        /// <param name="galleryName">Gallery name.</param>
        public static void SaveGallery(string galleryName)
        {
            ImageManager.GalleryName = galleryName;

            List<TransformableImage> images = ImageManager.Images;
            ImageSaveData[] imageSaveData = new ImageSaveData[ImageManager.Images.Count];

            for (int i = 0, count = images.Count; i < count; i++)
            {
                TransformableImage currentImage = images[i];
                Transform currentImageTransform = currentImage.transform;

                imageSaveData[i] = new ImageSaveData(currentImage.ID, currentImageTransform.position.x, currentImageTransform.position.y,
                                                     currentImageTransform.eulerAngles.z, currentImageTransform.localScale.x);
            }

            SaveData.AddGallerySaveData(new GallerySaveData(galleryName, System.DateTime.Now, imageSaveData));

            Debug.Log("Save file path: " + saveFilePath);

            using (var fileStream = new FileStream(saveFilePath, FileMode.Create))
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(fileStream, SaveData);
            }
        }

        #endregion

        #region Load

        /// <summary>
        /// Loads gallery save data from disk.
        /// </summary>
        /// <returns>
        /// A <see cref="SaveData"/> object.
        /// </returns>
        public static SaveData LoadGalleryData()
        {
            if (!File.Exists(saveFilePath))
            {
                SaveData = new SaveData();
                return SaveData;
            }
            else
            {
                using (var fileStream = new FileStream(saveFilePath, FileMode.Open))
                {
                    var binaryFormatter = new BinaryFormatter();
                    SaveData = (SaveData)binaryFormatter.Deserialize(fileStream);
                }

                return SaveData;
            }
        }

        #endregion
    }
}