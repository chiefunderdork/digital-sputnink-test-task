﻿using System.Collections.Generic;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Manages a collection of <see cref="TransformableImage"/> objects.
    /// </summary>
    public class ImageManager : MonoBehaviour
    {
        #region Properties

        /// <summary>
        /// Name of current loaded gallery, if any.
        /// </summary>
        public static string GalleryName { get; set; }

        /// <summary>
        /// Transform of the object that serves as a holder for the images.
        /// </summary>
        public static Transform ImageHolder { get; set; }

        /// <summary>
        /// List of images currently in the gallery.
        /// </summary>
        public static List<TransformableImage> Images { get; set; }

        /// <summary>
        /// Scriptable objects that contain configuration data for transformable images.
        /// </summary>
        public static TransformableImageConfig[] TransformableImageConfigs { get; set; } = null;

        /// <summary>
        /// Returs True when one of the handles on any of the spawned images is being dragged.
        /// </summary>
        public static bool HandleOnOneOfTheImagesIsBeingDragged
        {
            get
            {
                foreach (TransformableImage image in Images)
                {
                    foreach (TransformableImageHandle transformableImageHandle in image.TransformableImageHandles)
                    {
                        if (transformableImageHandle.IsBeingDragged)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Are spawned images currently showing debug info?
        /// </summary>
        private static bool isShowingDebugInfo = false;

        #endregion

        #region Constructor

        private void Start() => Images = new List<TransformableImage>();

        #endregion

        #region Spawn

        /// <summary>
        /// Spawns a transformable image from passed image ID.
        /// </summary>
        public static TransformableImage SpawnTransformableImage(int transformableImageID, Vector3 position,
                                                                 Quaternion rotation = default, float scale = 1f)
        {


            Debug.LogFormat("Spawning with rotation {0} and scale {1}", rotation, scale);
            GameObject imagePrefab = GetImagePrefab(transformableImageID);
            return SpawnTransformableImage(imagePrefab, position, rotation, scale);
        }

        /// <summary>
        /// Spawns a transformable image from passed image config.
        /// </summary>
        /// <param name="transformableImageConfig">Scriptable object that holds configuration data for a <see cref="TransformableImage"/>.</param>
        /// <param name="position">Image position.</param>
        /// <returns>
        /// A <see cref="TransformableImage"/>.
        /// </returns>
        public static TransformableImage SpawnTransformableImage(TransformableImageConfig transformableImageConfig, Vector3 position, 
                                                                 Quaternion rotation = default, float scale = 1f)                                                                 
        {
            return SpawnTransformableImage(transformableImageConfig.Prefab, position, rotation, scale);
        }

        /// <summary>
        /// Spawns a transformable image from passed prefab.
        /// </summary>
        /// <param name="transformableImagePrefab"><see cref="TransformableImage"/> prefab.</param>
        /// <param name="position">Image position.</param>
        /// <returns>
        /// A <see cref="TransformableImage"/>.
        /// </returns>
        public static TransformableImage SpawnTransformableImage(GameObject transformableImagePrefab, Vector3 position, 
                                                                 Quaternion rotation = default, float scale = 1f, bool instantiatedFromUndoRedoStack = false)
        {
            position.z = 0f;
            GameObject newImage = Instantiate(transformableImagePrefab, position, rotation, ImageHolder);
            newImage.transform.localScale = Vector3.one * scale;
            TransformableImage transformableImage = newImage.GetComponent<TransformableImage>();
            Images.Add(transformableImage);

            if (!instantiatedFromUndoRedoStack)
            {
                transformableImage.StartDragAfterInstantiatingCoroutine();
            }
            
            SortUp(transformableImage);

            if (isShowingDebugInfo)
            {
                transformableImage.ShowDebugInfo();
            }

            return transformableImage;
        }

        /// <summary>
        /// Adds an image to the list of images.
        /// </summary>
        /// <param name="transformableImage">Image to add.</param>
        /// <param name="imageIndex">Target index.</param>
        public void AddImageToPosition(TransformableImage transformableImage, int imageIndex)
        {
            var updatedList = new List<TransformableImage>();

            for (int i = 0; i < imageIndex - 1; i++)
            {
                updatedList.Add(Images[i]);
            }

            updatedList.Add(transformableImage);

            for (int i = imageIndex; i < Images.Count; i++)
            {
                updatedList.Add(Images[i]);
            }

            Images = updatedList;

            ResortAll();
        }

        /// <summary>
        /// Gets the image prefab that corresponds with the passed ID.
        /// </summary>
        /// <param name="imageID">Image ID.</param>
        /// <returns>
        /// A game object.
        /// </returns>
        private static GameObject GetImagePrefab(int imageID)
        {
            foreach (TransformableImageConfig transformableImageConfig in TransformableImageConfigs)
            {
                if (transformableImageConfig.ID == imageID)
                {
                    return transformableImageConfig.Prefab;
                }
            }

            return null;
        }

        #endregion

        #region Sort      

        /// <summary>
        /// Sorts the image to the top of the sorting order and adjusts the sorting order of other images accordingly.
        /// </summary>
        /// <param name="imageToSort"></param>
        public static void SortUp(TransformableImage imageToSort)
        {
            int imageIndex = Images.IndexOf(imageToSort);
            Images.RemoveAt(imageIndex);
            Images.Add(imageToSort);

            for (int imageCount = Images.Count; imageIndex < imageCount; imageIndex++)
            {
                Images[imageIndex].SetSortingLayerOrder(imageIndex);
            }
        }        

        /// <summary>
        /// Sets the sorting layer order on all images.
        /// </summary>
        private static void ResortAll()
        {
            for (int i = 0, count = Images.Count; i < count; i++)
            {
                Images[i].SetSortingLayerOrder(i);
            }
        }

        #endregion

        #region Destroy

        /// <summary>
        /// Destroys an image.
        /// </summary>
        /// <param name="transformableImage">Transformable image to destroy.</param>
        public static void DestroyImage(TransformableImage transformableImage, bool addToUndoRedoStack = true)
        {
            transformableImage.IsBeingDestroyed = true;

            if (addToUndoRedoStack)
            {
                Transform imageTransform = transformableImage.transform;
                UndoRedoStack.AddAction(new DestroyUndoRedoAction(transformableImage, imageTransform.position, imageTransform.rotation,
                                        imageTransform.localScale.x, UndoRedoActionType.Destroy, imageTransform));
            }

            Images.Remove(transformableImage);
            Destroy(transformableImage.gameObject);
            ResortAll();
        }

        /// <summary>
        /// Destroys all images.
        /// </summary>
        public static void DestroyAll()
        {
            for (int i = 0; i < Images.Count; i++)
            {
                Destroy(Images[i].gameObject);                
            }

            Images.Clear();
        }

        #endregion

        #region Debug

        /// <summary>
        /// Toggles debug info on spawned images.
        /// </summary>
        public static void ToggleDebugInfo()
        {
            isShowingDebugInfo = !isShowingDebugInfo;

            foreach (TransformableImage image in Images)
            {
                if (isShowingDebugInfo)
                {
                    image.ShowDebugInfo();
                }
                else
                {
                    image.HideDebugInfo();
                }
            }

        }

        #endregion
    }
}