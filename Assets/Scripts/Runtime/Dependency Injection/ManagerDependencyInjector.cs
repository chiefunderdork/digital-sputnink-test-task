﻿using DigitalSputnikTestTask.ScriptableObjects;
using DigitalSputnikTestTask.Events;
using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Injects dependencies into various manager objects.
    /// </summary>
    public class ManagerDependencyInjector : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Transform of the object that serves as an image holder.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Transform of the object that serves as an image holder.")]
        [SerializeField]
        private Transform imageHolder = null;

        /// <summary>
        /// RectTransform component on the slide out menu.
        /// </summary>
        [Tooltip("RectTransform component on the slide out menu.")]
        [SerializeField]
        private RectTransform slideOutMenuRectTransform = null;

        /// <summary>
        /// Image used as an interaction hint for when player drags the image or its handles.
        /// </summary>
        [Tooltip("Image used as an interaction hint for when player drags the image or its handles.")]
        [SerializeField]
        private Image interactionHint = null;

        /// <summary>
        /// Transform that markes the edge of the slide out menu.
        /// </summary>
        [Tooltip("Transform that markes the edge of the slide out menu.")]
        [SerializeField]
        private Transform slideOutPanelEdgeMarker = null;

        /// <summary>
        /// Scriptable object that holds the position of the player's input on current frame.
        /// </summary>
        [Space]
        [Header("Assets")]
        [Space]
        [Tooltip("Scriptable object that holds the position of the player's input on current frame.")]
        [SerializeField]
        private Vector2Variable currentInputPosition = null;

        /// <summary>
        /// Scriptable objects that contain configuration data for transformable images.
        /// </summary>
        [Tooltip("Scriptable objects that contain configuration data for transformable images.")]
        [SerializeField]
        private TransformableImageConfig[] transformableImageConfigs = null;

        /// <summary>
        /// Event that is raised when the undo stack is updated.
        /// </summary>
        [Space]
        [Header("Events")]
        [Space]
        [Tooltip("Event that is raised when the undo stack is updated.")]
        [SerializeField]
        private  GameEvent onUndoStackUpdated = null;

        /// <summary>
        /// Event that is raised when the redo stack is updated.
        /// </summary>
        [Tooltip("Event that is raised when the redo stack is updated.")]
        [SerializeField]
        private GameEvent onRedoStackUpdated = null;

        #endregion

        #region Injection

        private void Awake()
        {
            ImageManager.ImageHolder = imageHolder;
            UndoRedoStack.OnUndoStackUpdated = onUndoStackUpdated;
            UndoRedoStack.OnRedoStackUpdated = onRedoStackUpdated;
            TransformableImage.SlideOutMenuRectTransform = slideOutMenuRectTransform;
            TransformableImage.InteractionHint = interactionHint;
            TransformableImage.SlideOutPanelEdgeMarker = slideOutPanelEdgeMarker;
            TransformableImageHandle.CurrentInputPosition = currentInputPosition;
            ImageManager.TransformableImageConfigs = transformableImageConfigs;
        }

        #endregion
    }
}