﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Injects the reference to the main camera into objects that depend on it.
    /// </summary>
    public class MainCameraInjector : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// App's main camera.
        /// </summary>
        [Tooltip("App's main camera.")]
        [SerializeField]
        private Camera mainCamera = null;

        #endregion

        #region Injection

        private void Awake()
        {
            TransformableImage.MainCamera = mainCamera;
            TransformableImageHandle.MainCamera = mainCamera;
            TransformableImageMock.MainCamera = mainCamera;
            ScreenZoneHoverDetector.MainCamera = mainCamera;
        }

        #endregion
    }
}