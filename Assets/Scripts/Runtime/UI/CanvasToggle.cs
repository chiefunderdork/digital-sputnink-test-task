﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Toggles a GUI canvas on and off.
    /// </summary>
    public class CanvasToggle : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Canvas to toggle.
        /// </summary>
        [Tooltip("Canvas to toggle.")]
        [SerializeField]
        private Canvas canvasToToggle = null;

        #endregion

        #region Toggle

        /// <summary>
        /// Enables target canvas.
        /// </summary>
        public void EnableCanvas() => canvasToToggle.enabled = true;

        /// <summary>
        /// Disables target canvas.
        /// </summary>
        public void DisableCanvas() => canvasToToggle.enabled = false;

        /// <summary>
        /// Toggles target canvas.
        /// </summary>
        public void ToggleCanvas() => canvasToToggle.enabled = !canvasToToggle.enabled;

        #endregion
    }
}