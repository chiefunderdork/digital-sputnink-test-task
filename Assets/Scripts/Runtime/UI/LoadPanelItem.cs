﻿using DigitalSputnikTestTask.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// An object that represents a saved gallery in the load panel.
    /// </summary>
    public class LoadPanelItem : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that displays the save gallery name.
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Component that displays the save gallery name.")]
        [SerializeField]
        private TextMeshProUGUI saveNameText = null;

        /// <summary>
        /// Component that displays the date and time the save was made.
        /// </summary>
        [Tooltip("Component that displays the date and time the save was made.")]
        [SerializeField]
        private TextMeshProUGUI dateTimeText = null;

        /// <summary>
        /// Component that handles operations with the menu that allows the player to load a saved image gallery.
        /// </summary>
        [Tooltip("Component that handles operations with the menu that allows the player to load a saved image gallery.")]
        [SerializeField]
        private LoadGalleryMenu loadGalleryMenu = null;

        /// <summary>
        /// Component that handles clicks on a load panel item.
        /// </summary>
        [Tooltip("Component that handles clicks on a load panel item.")]
        [SerializeField]
        private Button button = null;

        /// <summary>
        /// Gallery save data stored in this item.
        /// </summary>
        private GallerySaveData attachedData = null;

        #endregion

        #region Constants

        /// <summary>
        /// String to use when no save data is available in a slot.
        /// </summary>
        private const string NoDataString = "No Data";

        #endregion

        #region Update

        /// <summary>
        /// Updates text data on tload panel item.
        /// </summary>
        /// <param name="gallerySaveData">Gallery save to use for updating data.</param>
        public void UpdateData(GallerySaveData gallerySaveData)
        {
            attachedData = gallerySaveData;

            if (gallerySaveData != null)
            {
                saveNameText.text = gallerySaveData.GalleryName;
                dateTimeText.text = gallerySaveData.CurrentDataTime.ToString("yyyy-MM-dd, hh:mm:ss");
                dateTimeText.enabled = true;
                button.enabled = true;
            }
            else
            {
                saveNameText.text = NoDataString;
                dateTimeText.enabled = false;
                button.enabled = false;
            }            
        }

        #endregion

        #region Load

        /// <summary>
        /// Loads the image gallery stored in <see cref="attachedData"/>.
        /// </summary>
        public void LoadGallery()
        {
            if (attachedData == null)
            {
                Debug.LogErrorFormat("{0} is trying to load null gallery data!", gameObject);
                return;
            }

            ImageManager.DestroyAll();

            ImageSaveData[] imageSaveData = attachedData.SavedImageData;

            foreach (ImageSaveData data in imageSaveData)
            {
                ImageManager.SpawnTransformableImage(data.ImageID, new Vector3(data.PositionX, data.PositionY, 0f),
                                                     Quaternion.Euler(0f, 0f, data.Rotation), data.Scale);
            }

            UndoRedoStack.ResetStacks();
            ImageManager.GalleryName = attachedData.GalleryName;
            loadGalleryMenu.Hide();
        }

        #endregion
    }
}