﻿using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles operations with a GUI menu.
    /// </summary>
    public class Menu : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Components that render button images.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [SerializeField]
        private Image[] buttonImages = null;

        /// <summary>
        /// Is this menu currently visible?
        /// </summary>
        protected bool isVisible = false;

        #endregion

        #region Toggle

        /// <summary>
        /// Toggles the menu.
        /// </summary>
        public virtual void Toggle()
        {
            isVisible = !isVisible;
            ToggleButtons(isVisible);
        }

        /// <summary>
        /// Shows the menu.
        /// </summary>
        public virtual void Show()
        {
            isVisible = true;
            ToggleButtons(isVisible);
        }

        /// <summary>
        /// Hides the menu.
        /// </summary>
        public virtual void Hide()
        {
            isVisible = false;
            ToggleButtons(isVisible);
        }

        #endregion

        #region Buttons

        /// <summary>
        /// Toggles menu button images on or off.
        /// </summary>
        /// <param name="toggleOn"></param>
        public void ToggleButtons(bool toggleOn)
        {
            var targetAlpha = toggleOn ? 1f : 0f;

            foreach (Image image in buttonImages)
            {
                image.raycastTarget = toggleOn;

                Color newColor = image.color;
                newColor.a = targetAlpha;
                image.color = newColor;
            }
        }

        #endregion
    }
}