﻿using DigitalSputnikTestTask.Events;
using TMPro;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles operations with the menu that allows the player to enter and confirm name for a saved gallery.
    /// </summary>
    public class SaveGalleryMenu : Menu
    {
        #region Properties

        /// <summary>
        /// Is this menu currently enabled?
        /// </summary>
        public static bool IsEnabled { get; private set; }

        #endregion

        #region Fields

        /// <summary>
        /// Component that displays a warning text when the user enters an invalid save gallery name.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Component that displays a warning text when the user enters an invalid save gallery name.")]
        [SerializeField]
        private TextMeshProUGUI invalidNameWarning = null;

        /// <summary>
        /// GUI component that displays the save name text in the input field.
        /// </summary>
        [Tooltip("GUI component that displays the save name text in the input field.")]
        [SerializeField]
        private TextMeshProUGUI saveNameField = null;

        /// <summary>
        /// Component that toggles a canvas.
        /// </summary>
        [SerializeField]
        private CanvasToggle canvasToggle = null;

        /// <summary>
        /// Event that is raised when the save process is started.
        /// </summary>
        [Space]
        [Header("Events")]
        [Space]
        [Tooltip("Event that is raised when the save process is started.")]
        [SerializeField]
        private GameEvent onStartedSaveProcess = null;

        #endregion

        #region Constants

        /// <summary>
        /// String to use as a placeholder for save name.
        /// </summary>
        private const string DefaultSaveNameText = "MYAWESOMEGALLERY";

        #endregion

        #region Save name

        /// <summary>
        /// Resets the save name in the input field to its default value.
        /// </summary>
        public void ResetSaveName()
        {
            Debug.Log("Image manager gallery name is null or empty: " + string.IsNullOrEmpty(ImageManager.GalleryName));
            saveNameField.text = string.IsNullOrEmpty(ImageManager.GalleryName) ? DefaultSaveNameText : ImageManager.GalleryName;
        }

        #endregion

        #region Toggle

        /// <summary>
        /// Toggles the menu.
        /// </summary>
        public override void Toggle()
        {
            base.Toggle();            

            if (!isVisible)
            {
                canvasToggle.DisableCanvas();
                ToggleInvalidNameWarning(false);
                IsEnabled = false;
            }
            else
            {
                canvasToggle.EnableCanvas();
                ResetSaveName();
                IsEnabled = true;
            }
        }        

        /// <summary>
        /// Hides the menu.
        /// </summary>
        public override void Hide()
        {
            base.Hide();
            ToggleInvalidNameWarning(false);
            canvasToggle.DisableCanvas();
            IsEnabled = false;
        }

        #endregion

        #region Hint

        /// <summary>
        /// Toggles the invalid save gallery name warning on and off.
        /// </summary>
        /// <param name="toggleOn">Should the warning be toggled on?</param>
        public void ToggleInvalidNameWarning(bool toggleOn) => invalidNameWarning.enabled = toggleOn;

        #endregion

        #region Buttons

        /// <summary>
        /// Tells the save system to save a gallery is a valid save name is provided in the input field.
        /// </summary>
        public void OnConfirmSaveNameButtonPressed()
        {
            string saveName = saveNameField.text;

            RemoveSpaces(ref saveName);

            if (!SaveNameIsValid(saveName))
            {
                ToggleInvalidNameWarning(true);
                return;
            }            

            onStartedSaveProcess.Raise();

            SaveManager.SaveGallery(saveName);
        }

        #endregion

        #region Validation

        /// <summary>
        /// Returns True is save name is valid, False otherwise.
        /// </summary>
        /// <param name="saveName">Save name.</param>
        /// <returns>
        /// A boolean.
        /// </returns>
        private bool SaveNameIsValid(string saveName)
        {
            if ((saveName.Length == 1 && (int)saveName[0] == 8203) || string.IsNullOrEmpty(saveName) || string.IsNullOrWhiteSpace(saveName))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Removes spaces from the string in case any were added.
        /// </summary>
        private void RemoveSpaces(ref string saveName)
        {
            saveName = string.Join("", saveName.Split(default(string[]), System.StringSplitOptions.RemoveEmptyEntries));
        }
        #endregion
    }
}