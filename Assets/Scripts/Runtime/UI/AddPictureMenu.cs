﻿using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles operations of the menu that is used to add pictures to the gallery.
    /// </summary>
    public class AddPictureMenu : Menu
    {
        #region Fields

        /// <summary>
        /// GUI components that draw mock images in the add picture menu.
        /// </summary>
        [SerializeField]
        private Image[] mockImages = null;

        #endregion

        #region Image mocks

        /// <summary>
        /// Toggles mock images on and off.
        /// </summary>
        /// <param name="toggleOn">Should the images be toggle on?</param>
        public void ToggleImageMocks(bool toggleOn)
        {
            foreach (Image mockImage in mockImages)
            {
                mockImage.enabled = toggleOn;
            }
        }

        #endregion
    }
}