﻿using DigitalSputnikTestTask.Events;
using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Raises an event when a button is pressed.
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class ButtonWithEvent : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Event that is raised when attached button is pressed.
        /// </summary>
        [Tooltip("Event that is raised when attached button is pressed.")]
        [SerializeField]
        private GameEvent onButtonPressed = null;

        #endregion

        #region Button

        /// <summary>
        /// Raises the event assigned in the <see cref="onButtonPressed"/> field when attached button is pressed.
        /// </summary>
        public void OnButtonPressed() => onButtonPressed.Raise();

        #endregion
    }
}