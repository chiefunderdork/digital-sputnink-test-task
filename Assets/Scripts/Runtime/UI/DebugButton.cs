﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles button that toggles the debug mode.
    /// </summary>
    public class DebugButton : MonoBehaviour
    {
        #region Button

        /// <summary>
        /// Tells the <see cref="ImageManager"/> to toggle debug info.
        /// </summary>
        public void ToggleDebugMode() => ImageManager.ToggleDebugInfo();

        #endregion
    }
}