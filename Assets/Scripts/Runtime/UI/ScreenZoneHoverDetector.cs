﻿using DigitalSputnikTestTask.Events;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Fires events whenever images are dragged into/out of a certain part of the screen.
    /// </summary>
    public class ScreenZoneHoverDetector : MonoBehaviour
    {
        #region Properties

        /// <summary>
        /// Apps's main camera.
        /// </summary>
        public static Camera MainCamera { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// Transform that represents the top left corner of the screen zone.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Transform that represents the top left corner of the screen zone.")]
        [SerializeField]
        private Transform screenZoneTopLeftCornerTransform = null;

        /// <summary>
        /// Transform that represents the bottom right corner of the screen zone.
        /// </summary>
        [Tooltip("Transform that represents the bottom right corner of the screen zone.")]
        [SerializeField]
        private Transform screenZoneBottomRightCornerTransform = null;

        /// <summary>
        /// Layer mask to use when detecting hovered images.
        /// </summary>
        [Space]
        [Header("Settings")]
        [Space]
        [Tooltip("Layer mask to use when detecting hovered images.")]
        [SerializeField]
        private LayerMask imageLayerMask;

        /// <summary>
        /// Event that is raised when an image is dragged into the screen zone.
        /// </summary>
        [Space]
        [Header("Events")]
        [Space]
        [Tooltip("Event that is raised when an image is dragged into the screen zone.")]
        [SerializeField]
        private GameEvent onDraggedImageIntoScreenZone = null;

        /// Event that is raised when an image is dragged outside the screen zone.
        /// </summary>
        [Tooltip("Event that is raised when an image is dragged outside the screen zone.")]
        [SerializeField]
        private GameEvent onDraggedImageOutsideScreenZone = null;

        /// <summary>
        /// Is any image currently hovered over the screen zone?
        /// </summary>
        private bool imageIsHoveredOverScreenZone = false;

        #endregion

        #region Hover detection

        private void Update()
        {
            if (Physics2D.OverlapArea(MainCamera.ScreenToWorldPoint(screenZoneTopLeftCornerTransform.position),
                                      MainCamera.ScreenToWorldPoint(screenZoneBottomRightCornerTransform.position),
                                      imageLayerMask) != null)
            {
                if (!imageIsHoveredOverScreenZone)
                {
                    onDraggedImageIntoScreenZone.Raise();
                    imageIsHoveredOverScreenZone = true;
                }
            }
            else
            {
                if (imageIsHoveredOverScreenZone)
                {
                    onDraggedImageOutsideScreenZone.Raise();
                    imageIsHoveredOverScreenZone = false;
                }
            }
        }

        #endregion
    }
}