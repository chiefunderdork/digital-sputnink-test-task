﻿using DigitalSputnikTestTask.Events;
using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Panel on the side of the screen that holds various menus.
    /// </summary>
    public class SlideOutPanel : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that animates the panel.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Component that animates the panel.")]
        [SerializeField]
        private Animator panelAnimator = null;

        /// <summary>
        /// Event that is raised when the slide out panel is toggled.
        /// </summary>
        [Tooltip("Event that is raised when the slide out panel is toggled.")]
        [SerializeField]
        private GameEvent onSlideOutPanelToggled = null;

        /// <summary>
        /// Is panel fully visible?
        /// </summary>
        private bool isFullyVisible = false;

        #endregion

        #region Constants

        /// <summary>
        /// Hash of the trigger that is used for the slide out animation.
        /// </summary>
        private int slideOutTrigger = Animator.StringToHash("SlideOut");

        /// <summary>
        /// Hash of the trigger that is used for the slide in animation.
        /// </summary>
        private int slideInTrigger = Animator.StringToHash("SlideIn");

        #endregion

        #region Animations

        /// <summary>
        /// Toggles the slide in/out animation.
        /// </summary>
        public void ToggleSlide()
        {
            panelAnimator.SetTrigger(isFullyVisible ? slideInTrigger: slideOutTrigger);
            isFullyVisible = !isFullyVisible;
            onSlideOutPanelToggled.Raise();
        }

        #endregion
    }
}