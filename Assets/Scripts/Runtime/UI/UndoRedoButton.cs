﻿using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Class to use for undo and redo buttons.
    /// </summary>
    public class UndoRedoButton : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// GUI component that handles player's clicks.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("GUI component that handles player's clicks.")]
        [SerializeField]
        private Button button = null;

        /// <summary>
        /// GUI component that draws the button image.
        /// </summary>
        [Tooltip("GUI component that draws the button image.")]
        [SerializeField]
        private Image image = null;

        #endregion

        #region Constants

        /// <summary>
        /// Alpha value to use when the button is disabled.
        /// </summary>
        private const float DisabledButtonAlpha = 0.35f;

        /// <summary>
        /// Alpha value to use when the button is enabled.
        /// </summary>
        private const float EnabledButtonAlpha = 1f;

        #endregion

        #region Toggle

        /// <summary>
        /// Calls the <see cref="Toggle(bool)"/> method when undo stack is updated. 
        /// </summary>
        public void OnUndoStackUpdated() => Toggle(UndoRedoStack.SizeOfUndoStack > 0);

        /// <summary>
        /// Calls the <see cref="Toggle(bool)"/> method when redo stack is updated. 
        /// </summary>
        public void OnRedoStackUpdated() => Toggle(UndoRedoStack.SizeOfRedoStack > 0);

        /// <summary>
        /// Toggles the button on or off.
        /// </summary>
        /// <param name="toggleOn">Should the button be toggled on?</param>
        private void Toggle(bool toggleOn)
        {
            button.interactable = toggleOn;

            Color newColor = image.color;
            newColor.a = toggleOn ? EnabledButtonAlpha : DisabledButtonAlpha;
            image.color = newColor;
        }

        #endregion

        #region Trigger

        /// <summary>
        /// Triggers the undo action on the <see cref="UndoRedoStack"/>.
        /// </summary>
        public void TriggerUndo() => UndoRedoStack.Undo();

        /// <summary>
        /// Triggers the redo action on the <see cref="UndoRedoStack"/>.
        /// </summary>
        public void TriggerRedo() => UndoRedoStack.Redo();

        #endregion
    }
}