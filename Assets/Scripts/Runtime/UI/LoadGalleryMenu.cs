﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Handles operations with the menu that allows the player to load a saved image gallery.
    /// </summary>
    public class LoadGalleryMenu : Menu
    {
        #region Properties

        /// <summary>
        /// Is this menu currently enabled?
        /// </summary>
        public static bool IsEnabled { get; private set; }

        #endregion

        #region Fields

        /// <summary>
        /// Components that contain text data for saved galleries.
        /// </summary>
        [Space]
        [Header("Components")]
        [Space]
        [Tooltip("Components that contain text data for saved galleries.")]
        [SerializeField]
        private List<LoadPanelItem> loadPanelItems = null;

        /// <summary>
        /// Component that renders GUI elements on the load gallery canvas.
        /// </summary>
        [Tooltip("Component that renders GUI elements on the load gallery canvas.")]
        [SerializeField]
        private Canvas loadGalleryCanvas = null;

        /// <summary>
        /// Component that allows to scroll the load panel.
        /// </summary>
        [Tooltip("Component that allows to scroll the load panel.")]
        [SerializeField]
        private Scrollbar scrollbar = null;        

        /// <summary>
        /// Prefab for a load panel item.
        /// </summary>
        [Space]
        [Header("Assets")]
        [Space]
        [Tooltip("Prefab for a load panel item.")]
        [SerializeField]
        private GameObject loadPanelItemPrefab = null;        

        #endregion

        #region Constants

        /// <summary>
        /// X coordinate of the first item in the load panel.
        /// </summary>
        private const float FirstItemXCoordinate = 200f;

        /// <summary>
        /// Load panel items' y coordinate.
        /// </summary>
        private const float YCoordinate = 28f;

        /// <summary>
        /// Distance between adjacent panel items.
        /// </summary>
        private const float DistanceBetweenItems = 600f;

        #endregion

        #region Init

        private void Start() => SaveManager.LoadGalleryData();

        #endregion

        #region Show/Hide

        /// <summary>
        /// Toggles the menu.
        /// </summary>
        public override void Toggle()
        {
            base.Toggle();

            if (isVisible)
            {
                UpdateLoadPanelItems();
                IsEnabled = true;
            }

            loadGalleryCanvas.enabled = isVisible;
        }

        /// <summary>
        /// Shows the load panel.
        /// </summary>
        public override void Show()
        {
            base.Show();
            UpdateLoadPanelItems();
            scrollbar.value = 0f;
            loadGalleryCanvas.enabled = IsEnabled = true;
        }

        /// <summary>
        /// Hides the load panel.
        /// </summary>
        public override void Hide()
        {
            if (!isVisible)
            {
                return;
            }

            base.Hide();
            loadGalleryCanvas.enabled = IsEnabled = false;
        }

        #endregion

        #region Panel

        /// <summary>
        /// Updates text data on load panel items.
        /// </summary>
        private void UpdateLoadPanelItems()
        {
            List<GallerySaveData> gallerySaveData = SaveManager.SaveData.GalerySaveData;

            AddItemsToThePanelIfRequired(gallerySaveData.Count);

            for (int i = 0, panelCount = loadPanelItems.Count, saveCount = gallerySaveData.Count; i < panelCount; i++)
            {
                if (i < saveCount)
                {
                    loadPanelItems[i].UpdateData(gallerySaveData[i]);
                }
                else
                {
                    loadPanelItems[i].UpdateData(null);
                }
            }
        }

        /// <summary>
        /// Adds items to the load panel if save number exceeds their number.
        /// </summary>
        private void AddItemsToThePanelIfRequired(int numberOfSavedGalleries)
        {
            int numberOfLoadPanelItems = loadPanelItems.Count;

            if (numberOfSavedGalleries > numberOfLoadPanelItems)
            {
                Transform loadPanelParent = loadPanelItems[0].transform.parent;
                float addedElementXCoordinate = FirstItemXCoordinate + numberOfLoadPanelItems * DistanceBetweenItems;

                int numberOfAddedPanels = numberOfSavedGalleries - numberOfLoadPanelItems;

                for (int i = 1; i <= numberOfAddedPanels; i++)
                {
                    GameObject newPanelItem = Instantiate(loadPanelItemPrefab, loadPanelParent);
                    loadPanelParent.localPosition = new Vector3(addedElementXCoordinate, YCoordinate, 0f);
                    loadPanelItems.Add(newPanelItem.GetComponent<LoadPanelItem>());
                    addedElementXCoordinate += DistanceBetweenItems;
                }
            }
        }
        #endregion
    }
}