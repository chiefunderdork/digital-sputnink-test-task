﻿using UnityEngine;

namespace DigitalSputnikTestTask.Events
{
    /// <summary>
    /// Stores a list of events to listen to and respective responses.
    /// </summary>
    [System.Serializable]
    public class GameEventListener : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// An array of listeners.
        /// </summary>
        [Tooltip("An array of listeners.")]
        [SerializeField]
        private EventResponsePair[] Listeners = null;

        #endregion

        #region Enable/disable

        private void Awake() => OnEnable();

        private void OnEnable()
        {
            if (!enabled)
            {
                return;
            }            

            if (Listeners == null)
            {
                return;
            }                

            foreach (EventResponsePair e in Listeners)
            {
                if (e.Event == null || e.Event.EventListeners == null || e.Event.EventListeners.Contains(this))
                {
                    continue;
                }

                e.Event?.RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (Listeners == null)
            {
                return;
            }

            foreach (EventResponsePair e in Listeners)
            {
                e.Event?.UnregisterListener(this);
            }
        }

        #endregion

        /*#if UNITY_EDITOR
                private void OnValidate()
                {
                    if (Listeners == null)
                    {
                        return;
                    }                

                    if (!gameObject.activeInHierarchy)
                    {
                        return;
                    }                

                    if (EditorApplication.isPlayingOrWillChangePlaymode)
                    {
                        return;
                    }                

                    foreach (EventResponsePair e in Listeners)
                    {
                        e.Event?.RegisterListener(this);
                    }
                }
        #endif*/

        #region Event

        /// <summary>
        /// Invokes listeners if their event's name matches the name of the passed argument.
        /// </summary>
        /// <param name="e">Target event.</param>
        public void OnEventRaised(GameEvent e)
        {
            foreach (EventResponsePair eventResponsePair in Listeners)
            {
                if (e.Equals(eventResponsePair.Event))
                {
                    
#if UNITY_EDITOR
                    if (eventResponsePair.Event.LogToConsole)
                    {
                        Debug.Log(string.Format("<b>{0}</b> is invoking <i>{1}</i> in response to <i>{2}</i> event", eventResponsePair.Response.GetPersistentTarget(0).name,
                                                                                                                    eventResponsePair.Response.GetPersistentMethodName(0),
                                                                                                                    eventResponsePair.Event.name));
                    }
#endif
                    eventResponsePair.Response.Invoke();
                }                
            }
        }

        #endregion
    }
}