﻿using System.Collections.Generic;
using UnityEngine;

namespace DigitalSputnikTestTask.Events
{ 
#if UNITY_EDITOR
    [CreateAssetMenu(fileName = "New Event", menuName = "Scriptable Objects/Events/Event", order = 2)]
#endif
    /// <summary>
    /// A scriptable object that holds a unique game event type.
    /// </summary>
    public class GameEvent : ScriptableObject
    {
        public string Name => name;

        /// <summary>
        /// "When set to True, will log each invokation connected to this event."
        /// </summary>
        [Tooltip("When set to True, will log each invokation connected to this event.")]
        public bool LogToConsole = true;

        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        public List<GameEventListener> EventListeners;

        /// <summary>
        /// Raises the event, notifying all listeners.
        /// is True.
        /// </summary>
        public void Raise()
        {
#if UNITY_EDITOR
            //Debug.LogFormat("Raising {0} with Doozy event: {1} and listener count: {2}", name, raiseDoozyUIEvent, EventListeners.Count);
#endif

            var index = EventListeners.Count - 1;
            
            for (; index >= 0; index--)
            {
                int numberOfListenersBeforeNotifyingCurrent = EventListeners.Count;
                GameEventListener currentListener = EventListeners[index];

                currentListener.OnEventRaised(this);                    

                if (EventListeners.Count != numberOfListenersBeforeNotifyingCurrent)
                {
                    if (!EventListeners.Contains(currentListener))
                    {
                        index = EventListeners.Count;
                    }
                    else
                    {
                        index = EventListeners.Count - 1;
                    }
                }
            }
        }

        /// <summary>
        /// Registers a new listeners.
        /// </summary>
        /// <param name="listener">Listener to register.</param>
        public void RegisterListener(GameEventListener listener)
        {
            if (EventListeners == null)
            {
                EventListeners = new List<GameEventListener>();
            }

            if (!EventListeners.Contains(listener))
            {
                EventListeners.Add(listener);               
            }                
        }

        /// <summary>
        /// Unregisters an existing listeners.
        /// </summary>
        /// <param name="listener">Listener to unregister.</param>
        public void UnregisterListener(GameEventListener listener)
        {
            if (EventListeners.Contains(listener))
            {
                EventListeners.Remove(listener);
            }                
        }

        private void OnEnable() => EventListeners = new List<GameEventListener>();
    }
}