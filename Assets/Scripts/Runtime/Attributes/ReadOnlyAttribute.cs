﻿using UnityEngine;

namespace DigitalSputnikTestTask
{
    /// <summary>
    /// Attribute for drawing fields that can't be changed in the inspector.
    /// </summary>
    public class ReadOnlyAttribute : PropertyAttribute { }

}